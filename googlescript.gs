
function Start() {

  var props = PropertiesService.getScriptProperties();
//twitter api auth
  props.setProperties({
    TWITTER_CONSUMER_KEY: "",
    TWITTER_CONSUMER_SECRET: "",
    TWITTER_ACCESS_TOKEN: "",
    TWITTER_ACCESS_SECRET: "",
    MAX_TWITTER_ID: 0
  });

  // Delete exiting triggers, if any

  var triggers = ScriptApp.getProjectTriggers();

  for (var i = 0; i < triggers.length; i++) {
    ScriptApp.deleteTrigger(triggers[i]);
     
  }

  // Setup a time-based trigger for the Bot to fetch and process incoming Tweets 
  // every minute. If your Google Script is running out of quota, change the
  // time to 5 or34 10 minutes though the bot won't offer real-time answers then.
  ScriptApp.newTrigger("Main")
    .timeBased()
    .everyMinutes(1)
    .create();

}


function Main() {

  try {

    var props = PropertiesService.getScriptProperties(),
      twit = new Twitter.OAuth(props);
    // Are the Twitter access tokens are valid?
    if (twit.hasAccess()) {
      var tweets = twit.fetchTweets("from:twit_led",
        function(tweet) {
          // Ignore tweets that are sensitive (NSFW content)
          if (!tweet.possibly_sensitive) {
            var answer = getanswer(tweet);
            Logger.log("Tweet: " + tweet.id_str + "Answer:" + answer);
            if (answer>=0) {
              return {
                answer: answer,
                id_str: tweet.id_str
              };
            }
          }
        }, {
          multi: true,
//          lang: "en",  // Fetch only English tweets
          count: 10,    // Process 10 tweets in a batch
          since_id: props.getProperty("MAX_TWITTER_ID")
        });

      if (tweets.length) {

       // The MAX_TWITTER_ID property store the ID of the last tweet answered by the bot
        props.setProperty("MAX_TWITTER_ID", tweets[0].id_str);

        // Process the tweets in FIFO order
        for (var i = tweets.length - 1; i >= 0; i--) {
          
          twit.favorite(tweets[i]);
          
          Logger.log("Tweet: " + tweets[i].answer.toString());
          
          sendfirebase(tweets[i].answer.toString() );
          
         // FirebaseApp.getDatabaseByUrl("https://tweetled-9c903.firebaseio.com", optSecret)
          // Wait a second to avoid hitting the rate limits
          //Utilities.sleep(1000);
        }
      }
    }

  } catch (f) {
    // You can also use MailApp to get email notifications of errors.
    Logger.log("Error: " + f.toString());
  }

}

function getanswer(tweet) {
var NOT_FOUND = -1;
  try {
    var resp  =NOT_FOUND;
    if (tweet.text.indexOf("ON") > NOT_FOUND) {
      resp = 1;
    }else if(tweet.text.indexOf("OFF") > NOT_FOUND) {
      resp = 0;
    }else{
      resp = NOT_FOUND;
    }
    return resp;
  } catch (f) {
    // You can also use MailApp to get email notifications of errors.
    Logger.log("Error: " + f.toString());
  }
  return false;
}

function sendfirebase (message) {
//firbase link and auth key
  var database = FirebaseApp.getDatabaseByUrl("", "");
  var data = { "flag": message };
  Logger.log(database.updateData("/", data));
 
}
